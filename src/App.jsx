import Chapter from './components/home/chapters';
import HeroSection from './components/home/hero-section';
import Instructor from './components/home/instructor';
import Section1 from './components/home/section-1';
import Section2 from './components/home/section-2';
import Section3 from './components/home/section-3';
import Testimonials from './components/home/testimonials';
import ResponsiveDrawer from './components/menu/drawer';

function App() {
  return (
    <div id='Ubuntu' className='flex flex-col gap-4'>
      <ResponsiveDrawer/>
      <HeroSection />
      <Section1/>
      <Section2/>
      <Chapter/>
      <Section3/>
      <Instructor/>
      <Testimonials/>

    </div>
  );
}

export default App;
