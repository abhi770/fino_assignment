
import { Check } from '@mui/icons-material'
import React from 'react'

const Section3 = () => {

    const course_designed_for = [
        'Students',
        'Job Aspirants',
        'Working Professionals',
        'Business Owners',
        'Housewives',
    ]


    return (

        <div className='flex flex-col gap-8 p-4 md:p-10 lg:p-20'>
            <div className='flex flex-col justify-center items-center'>
                <h1 className='text-3xl text-center font-bold  text-primary'>This Course is Designed For...!</h1>
                <div className='w-[50%] md:w-[25%] h-1 bg-secondary mt-2'></div>
            </div>

            <div className='grid grid-cols-1 md:grid-cols-3 gap-4'>
                <div className='flex flex-col gap-4 col-span-2'>
                    <h1 className='text-2xl col-span-2 font-bold'>
                    Anyone who wants to know how to analyse a company and pick 
                    </h1>
                    <button className='p-2 font-bold bg-[#FABF35CC] w-48 mx-auto'>
                        THE RIGHT STOCKS!
                    </button>
                </div>
                <div className='col-span-1 flex flex-col gap-4 '>
                    {course_designed_for.map((text, index) => (
                        <div key={index} className='flex gap-4'>
                            <div className='p-0 rounded-md bg-primary '>
                                <Check sx={{ color: 'white' }} />
                            </div>
                            <p>{text}</p>
                        </div>
                    ))}
                </div>
            </div>

        </div>

    )
}

export default Section3