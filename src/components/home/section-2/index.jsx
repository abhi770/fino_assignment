import { BrandingWatermark, EmojiObjects, Storage, TouchAppSharp } from '@mui/icons-material'
import { Container } from '@mui/material'
import React from 'react'

const Section2 = () => {
    const cards_text = [
        'How To Pick Right Stock?',
        'How To Do Valuation Of a Company?',
        'How To Do Fundamental Analysis Of Companies?',
        'How To Avoid Bad Stock?',
        'How To Start Investing Immediately ?'

    ]
    return (
        <Container>
            <div className='flex flex-col gap-8 py-8'>
                <div className='flex flex-col justify-center items-center'>
                    <h1 className='text-3xl text-center font-bold text-primary '>Join The Course & Get To Know</h1>
                    <div className='w-[50%] md:w-[25%] h-1 bg-secondary mt-2'></div>
                </div>

                <div className='flex justify-center items-center flex-col gap-4'>
                    {cards_text.map((text, index) => (
                        <div key={index} className='flex items-center bg-white shadow-xl w-full md:w-[60%] p-4 gap-4 font-bold'>
                            <div className=' p-[5px] rounded-md '>
                                <EmojiObjects sx={{ color: '#FABF35', fontSize: 64 }} />
                            </div>
                            <p>{text}</p>
                        </div>
                    ))
                    }
                </div>
                <div className='flex justify-center'>
                    <button className='text-lg md:text-2xl font-bold bg-gradient-to-tl from-primary to-[#352A9AA1] rounded-full text-white p-1 py-4 md:py-6 px-4  md:px-12 mt-6'>GET YOUR ACCESS NOW</button>
                </div>
            </div>
        </Container>
    )
}

export default Section2