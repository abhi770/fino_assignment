import React, { useState } from 'react';
import './index.css'; // Create this CSS file for custom styling



const content = [
    "Welcome to our Online Learning Platform. Learn new skills and explore a world of knowledge. Our courses cover a wide range of subjects, from programming to design, data science, and much more. With our expert instructors and interactive learning materials, you can achieve a lot of things.",
    "Unlock your creativity with our Graphic Design courses. Learn the principles of design, typography, and image editing tools. From creating logos to designing stunning visuals, our courses will help you become a skilled graphic designer and bring your creative ideas to life.",
    "Master the art of Mobile App Development and build your own apps. Our courses cover iOS and Android app development, using languages like Swift and Kotlin. Whether you want to create a game, utility app, or social platform, our courses will guide you through the process.",
    "Learn about Data Science and how to analyze complex datasets. From data wrangling to machine learning models and data visualization, our courses will equip you with the skills to make data-driven decisions. Data Science is a sought-after skill in various industries, and our courses will help you stand out.",
    "Explore the realm of Cybersecurity and protect against digital threats. Our courses cover ethical hacking, network security, and cybersecurity best practices. Join us to become a cybersecurity expert and safeguard digital assets from potential risks.",
];
const Chapter = () => {
    const handleButtonClick = (index) => {
        setActiveIndex(index);
    };

    const [activeIndex, setActiveIndex] = useState(0);
    return (
        <div className="custom-carousel-container flex flex-col gap-4 md:gap-8">
            <div className='flex flex-col justify-center items-center'>
                <h1 className='text-3xl text-center font-bold text-primary '>Course Syllabus</h1>
                <div className='w-[80%]  h-1 bg-secondary mt-2'></div>
            </div>
            <div className="chapter-buttons gap-4 md:gap-16">
                <h1 className='bg-secondary   text-black p-4 rounded-full font-bold px-16'>Chapters</h1>
                <div>
                {content.map((_, index) => (
                    <button
                        key={index}
                        className={activeIndex === index ? 'active' : 'inactive'}
                        onClick={() => handleButtonClick(index)}
                    >
                        {index + 1}
                    </button>
                ))}
                </div>
            </div>

            <div className='main-content-course flex gap-8'>
                <div className='blur-content'>
                    <p>    "Lorem ipsum dolor sit amet. Aut quos laborum ut animi cumque qui quis vero id fugiat culpa sed natus pariatur ut autem repellendus! Ab error sunt qui deserunt quae aut repellendus molestias. Et dolores nihil id omnis nobis aut galisum rerum At nemo corporis. Et velit aliquam rem autem excepturi et debitis quia sit magni dolorem sed minima laudantium in perspiciatis internos ut delectus impedit?",
                    </p>
                </div>
                <div className='active-content flex flex-col gap-4'>

                    <div className='!h-auto'>
                        <div className='flex justify-between'>
                            <button className='bg-primary rounded-full p-1 px-2 text-white'>CH : {activeIndex+1}</button>
                        <span className='duration'>Duration: 2 hrs</span>
                        </div>

                    </div>
                    <div className='card-bottom '>
                        <h4 className='text-xl font-bold my-2' >This is the heading</h4>

                        <span>{content[activeIndex]}</span>


                    </div>
                </div>
                <div className='blur-content'>
                    <p>    "Lorem ipsum dolor sit amet. Aut quos laborum ut animi cumque qui quis vero id fugiat culpa sed natus pariatur ut autem repellendus! Ab error sunt qui deserunt quae aut repellendus molestias. Et dolores nihil id omnis nobis aut galisum rerum At nemo corporis. Et velit aliquam rem autem excepturi et debitis quia sit magni dolorem sed minima laudantium in perspiciatis internos ut delectus impedit?",
                    </p>
                </div>

            </div>
            <div className='flex gap-2  mx-auto w-full justify-center '>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-gray-400 rounded-full'></div>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-primary rounded-full'></div>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-gray-400 rounded-full'></div>
                        </div>


        </div>
    );
};

export default Chapter;