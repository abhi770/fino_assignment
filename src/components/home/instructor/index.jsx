
import { Check, Facebook, Instagram, LinkedIn, Twitter, YouTube } from '@mui/icons-material'
import { IconButton } from '@mui/material'
import React from 'react'
import user from '../../../assets/user.png'
const Instructor = () => {

    const course_designed_for = [
        'Students',
        'Job Aspirants',
        'Working Professionals',
        'Business Owners',
        'Housewives',
    ]


    return (

        <div className='flex flex-col gap-8 p-4  lg:p-20 justify-center items-center'>
            <div className='flex flex-col justify-center items-center'>
                <h1 className='text-2xl text-center font-bold  '>Meet your Instructor</h1>
                <h1 className='text-3xl text-center font-bold text-primary  '>Prasad R. Lendwe</h1>
                <div className='w-[50%] md:w-[25%] h-1 bg-secondary mt-2'></div>
            </div>
            <div className='border-b-4 border-r-4 border-secondary '>
                <div className='shadow-xl p-10 grid grid-cols-1 md:grid-cols-3 gap-4 m-4 '>
                    <div className='col-span-1 gap-4 flex flex-col items-center md:translate-y-[-150px]'>
                        <div className='flex justify-center items-center w-72 h-72 md:w-80 md:h-80 bg-yellow-400 overflow-auto  rounded-full'>
                            <img className='object-cover w-full h-full' alt='User' src={user} />
                        </div>
                        <div className='flex gap-2'>
                            <IconButton><YouTube /></IconButton>
                            <IconButton><Instagram /></IconButton>
                            <IconButton><LinkedIn /></IconButton>
                            <IconButton><Facebook /></IconButton>
                            <IconButton><Twitter /></IconButton>
                        </div>

                    </div>
                    <div className='col-span-2 grid grid-cols-1 md:grid-cols-3 gap-6'>
                        <div className='p-2'>
                            <h6 className='text-lg text-primary font-bold'>10+ Years</h6>
                            <p className=''>Experience in Stock Market</p>
                        </div>

                        <div className='p-2'>
                            <h6 className='text-lg text-primary font-bold'>2Mn +</h6>
                            <p className=''>Subscribers (YouTube)</p>
                        </div>

                        <div className='p-2'>
                            <h6 className='text-lg text-primary font-bold'>17.5% CAGR</h6>
                            <p className=''>Personal Portfolio</p>
                        </div>

                        <div className='p-2'>
                            <h6 className='text-lg text-primary font-bold'>8+ Years</h6>
                            <p className=''>Teaching Experience</p>
                        </div>

                        <div className='p-2'>
                            <h6 className='text-lg text-primary font-bold'>147+ Hours</h6>
                            <p className=''>Content Created on Indian Stock Market</p>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    )
}

export default Instructor