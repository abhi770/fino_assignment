
import { BrandingWatermark, ChevronRight, Storage, TouchAppSharp } from '@mui/icons-material'
import { Container } from '@mui/material'
import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCards } from 'swiper/modules';
import './swiper.css';
import 'swiper/css';
import 'swiper/css/effect-cards';
import user from '../../../assets/profile.png'
import commas from '../../../assets/commas.png'
// import 'swiper/css/effect-cards';

const Testimonials = () => {

    const testimonialList = [
        {
            url: "https://www.youtube.com/embed/-hWQaocM1sY",
            heading: "Didn’t know that I can be able to make returns!",
            description: "This class will teach you to pick the profitable stocks that hold the future in them. I am glad that I got a chance to....",
            picture: "../img/instructor.png",
            name: "Pooja Mishra",
            title: "Investor",
            date: "19th June, 2023.",
        },
        {
            url: "https://www.youtube.com/embed/-hWQaocM1sY",
            heading: "Didn’t know that I can be able to make returns!",
            description: "This class will teach you to pick the profitable stocks that hold the future in them. I am glad that I got a chance to....",
            picture: "../img/instructor.png",
            name: "Pooja Mishra",
            title: "Investor",
            date: "19th June, 2023.",
        },
        {
            url: "https://www.youtube.com/embed/-hWQaocM1sY",
            heading: "Didn’t know that I can be able to make returns!",
            description: "This class will teach you to pick the profitable stocks that hold the future in them. I am glad that I got a chance to....",
            picture: "../img/instructor.png",
            name: "Pooja Mishra",
            title: "Investor",
            date: "19th June, 2023.",
        },


    ];


    return (
        <Container>
            <div className='flex flex-col gap-8 py-8 overflow-x-hidden'>
                <div className='flex flex-col justify-center items-center'>
                    <h1 className='text-3xl text-center font-bold text-primary '>Testimonials</h1>
                    <div className='w-[15%] h-1 bg-secondary mt-2'></div>
                </div>
                <div className='relative'>
                    <img src={commas} className='invisible md:visible absolute top-4  left-[20%] w-32' />
                    <div className='relative'>
                    <div className='my-24 flex justify-center '>
                        <div className='w-[400px] shadow-xl rounded-lg overflow-hidden '>
                            <div className='h-[240px]  flex flex-col justify-center items-center bg-black'>
                                <iframe
                                    className="w-full h-full"
                                    title="YouTube Video"
                                    src={testimonialList[0].url}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                ></iframe>
                            </div>
                            <div className='flex flex-col gap-8'>
                                <div className='text-black p-4 flex flex-col gap-6'>
                                    <h5 className='text-lg font-bold'>{testimonialList[0].heading}</h5>
                                    <p className='text-sm '>{testimonialList[0].description} <a className='text-[#352A9A]'>Read more</a></p>
                                </div>

                                <div className='text-black flex gap-2  items-center p-4'>
                                    <div className='w-12 h-12  rounded-full overflow-hidden flex-1 '>
                                        <img className='object-cover' alt='User' src={user} />
                                    </div>
                                    <div className='flex-1'>
                                        <h5 className='text-sm'>{testimonialList[0].name}</h5>
                                        <p className='text-xs'>{testimonialList[0].title}</p>
                                    </div>
                                    <h5 className='text-sm text-[#010851]'>{testimonialList[0].date}</h5>
                                </div>
                            </div>
                        </div>


                        {/* <Swiper
                        effect={'cards'}
                        grabCursor={true}
                        modules={[EffectCards]}
                        className="mySwiper"
                    > */}



                        {/* {testimonialList.map((item, i) => (
                            <SwiperSlide className='bg-white shadow-xl  overflow-hidden'>
                                <div className='h-[240px] flex flex-col justify-center items-center bg-black'>
                                    <iframe
                                        className="w-full h-full"
                                        title="YouTube Video"
                                        src={item.url}
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen
                                    ></iframe>
                                </div>
                                <div className='text-black p-4'>
                                    <h5 className='text-lg'>{item.heading}</h5>
                                    <p className='text-sm'>{item.description} <a>Read more</a></p>
                                </div>

                                <div className='text-black flex gap-2  items-center p-4'>
                                    <div className='w-12 h-12  rounded-full overflow-hidden '>
                                        <img className='object-cover' alt='User' src={user} />
                                    </div>
                                    <div className='flex-1'>
                                        <h5 className='text-sm'>{item.name}</h5>
                                        <p className='text-xs'>{item.title}</p>
                                    </div>
                                    <h5 className='text-sm text-[#010851]'>{item.date}</h5>
                                </div>
                            </SwiperSlide>
                        ))} */}


                        {/* </Swiper> */}
                    </div>

                    <div className='w-[px] md:w-[400px] shadow-xl rounded-lg overflow-hidden absolute top-0 bg-white left-[4%] rotate-2 md:rotate-3 md:left-[32%] '>
                            <div className='h-[240px]  flex flex-col justify-center items-center bg-black'>
                                <iframe
                                    className="w-full h-full"
                                    title="YouTube Video"
                                    src={testimonialList[0].url}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                ></iframe>
                            </div>
                            <div className='flex flex-col gap-8'>
                                <div className='text-black p-4 flex flex-col gap-6'>
                                    <h5 className='text-lg font-bold'>{testimonialList[0].heading}</h5>
                                    <p className='text-sm '>{testimonialList[0].description} <a className='text-[#352A9A]'>Read more</a></p>
                                </div>

                                <div className='text-black flex gap-2  items-center p-4'>
                                    <div className='w-12 h-12  rounded-full overflow-hidden '>
                                        <img className='object-cover' alt='User' src={user} />
                                    </div>
                                    <div className='flex-1'>
                                        <h5 className='text-sm'>{testimonialList[0].name}</h5>
                                        <p className='text-xs'>{testimonialList[0].title}</p>
                                    </div>
                                    <h5 className='text-sm text-[#010851]'>{testimonialList[0].date}</h5>
                                </div>
                            </div>
                        </div>


                    <div className='w-[px] md:w-[390px] shadow-xl rounded-lg overflow-hidden absolute top-0 bg-white left-[4%] rotate-3 md:rotate-[-14px] md:left-[32%] '>
                            <div className='h-[240px]  flex flex-col justify-center items-center bg-black'>
                                <iframe
                                    className="w-full h-full"
                                    title="YouTube Video"
                                    src={testimonialList[0].url}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                ></iframe>
                            </div>
                            <div className='flex flex-col gap-8'>
                                <div className='text-black p-4 flex flex-col gap-6'>
                                    <h5 className='text-lg font-bold'>{testimonialList[0].heading}</h5>
                                    <p className='text-sm '>{testimonialList[0].description} <a className='text-[#352A9A]'>Read more</a></p>
                                </div>

                                <div className='text-black flex gap-2  items-center p-4'>
                                    <div className='w-12 h-12  rounded-full overflow-hidden '>
                                        <img className='object-cover' alt='User' src={user} />
                                    </div>
                                    <div className='flex-1'>
                                        <h5 className='text-sm'>{testimonialList[0].name}</h5>
                                        <p className='text-xs'>{testimonialList[0].title}</p>
                                    </div>
                                    <h5 className='text-sm text-[#010851]'>{testimonialList[0].date}</h5>
                                </div>
                            </div>
                        </div>

                        <div className='w-[px] md:w-[390px] shadow-xl rounded-lg overflow-hidden absolute top-0 bg-white left-[4%] md:left-[33%] rotate-[-20px]  '>
                            <div className='h-[240px]  flex flex-col justify-center items-center bg-black'>
                                <iframe
                                    className="w-full h-full"
                                    title="YouTube Video"
                                    src={testimonialList[0].url}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                ></iframe>
                            </div>
                            <div className='flex flex-col gap-8'>
                                <div className='text-black p-4 flex flex-col gap-6'>
                                    <h5 className='text-lg font-bold'>{testimonialList[0].heading}</h5>
                                    <p className='text-sm '>{testimonialList[0].description} <a className='text-[#352A9A]'>Read more</a></p>
                                </div>

                                <div className='text-black flex gap-2  items-center p-4'>
                                    <div className='w-12 h-12  rounded-full overflow-hidden '>
                                        <img className='object-cover' alt='User' src={user} />
                                    </div>
                                    <div className='flex-1'>
                                        <h5 className='text-sm'>{testimonialList[0].name}</h5>
                                        <p className='text-xs'>{testimonialList[0].title}</p>
                                    </div>
                                    <h5 className='text-sm text-[#010851]'>{testimonialList[0].date}</h5>
                                </div>
                            </div>
                        </div>

                        <div className='bg-yellow-100 invisible md:visible cursor-pointer absolute w-12 h-12 top-[4%] right-0 rounded-full flex justify-center items-center'>
                            <ChevronRight fontSize='large' />
                        </div>

                       



                    
                    </div>
                    <img src={commas} className=' invisible md:visible  absolute bottom-10 rotate-180 right-[20%] w-32' />
                    <div className='flex gap-2  mx-auto w-full justify-center '>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-gray-400 rounded-full'></div>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-primary rounded-full'></div>
                            <div className='w-[18px] h-[18px] cursor-pointer  bg-gray-400 rounded-full'></div>
                        </div>
                </div>
            </div>
        </Container >
    )
}

export default Testimonials