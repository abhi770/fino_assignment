import { AccessTime, Group, Star } from '@mui/icons-material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import React from 'react'

const HeroSection = () => {
  return (
    <div id='Ubuntu' className='grid grid-cols-1 md:grid-cols-2 gap-4 p-4 md:px-10 lg:px-20 '>
        <div className=' sm:p-4 md:p-10 flex flex-col gap-8 order-2 md:order-1'>
            <div>
            <button className='bg-[#FABF35] flex  p-2 rounded-md font-bold'>
            Fundamental Analysis Course (Hindi)
            <KeyboardArrowDownIcon/>
            </button>
            </div>
            <div className='flex flex-col gap-0'>
            <h1 className='text-[40px] font-bold'>The Complete</h1>
            <h1 className='text-[40px] font-bold text-[#352A9A]'>Fundamental Analysis</h1>
            <h1 className='text-[40px] font-bold'>Course in Hindi</h1>
            </div>
            <div>
            <button className='text-2xl font-bold bg-[#352A9A] rounded-full text-white p-1 py-2 px-4 md:px-24 mx-auto mt-6'>BUY NOW</button>
            </div>

            <div className='flex gap-2'>
                <div className='flex gap-4'>
                    <AccessTime/>
                    2 Hours
                </div>

                <div className='flex gap-4'>
                    <Group/>
                    3K Enrolled
                </div>

                <div className='flex gap-4'>
                    <Star/>
                    4.4
                </div>
            </div>
        </div>
        <div className=' sm:p-4 md:p-10 flex items-center justify-center  order-1 md:order-2'>
        <iframe
            title="YouTube Video"
            className='w-[100%] md:w-[90%] h-[320px] sm:h-[400px] md:h-[70%]'
            src="https://www.youtube.com/embed/_DwmKtbVFJ4?start=2412"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>

        </div>
    </div>
  )
}

export default HeroSection