import { BrandingWatermark, Storage, TouchAppSharp } from '@mui/icons-material'
import { Container } from '@mui/material'
import React from 'react'

const Section1 = () => {
    return (
        <Container>
            <div className='flex flex-col gap-8 py-8'>
                <div className='flex flex-col justify-center items-center'>
                    <h1 className='text-3xl text-center font-bold  text-primary'>Main Features of The Course</h1>
                    <div className='w-[50%] md:w-[25%] h-1 bg-secondary mt-2'></div>
                </div>

                <div className='flex justify-center items-center flex-col gap-4'>
                    <div className='flex items-center bg-white shadow-xl w-full md:w-[60%] p-4 gap-4 font-bold'>
                        <div className='bg-primary p-[5px] rounded-md '>
                            <Storage fontSize='large' sx={{ color: 'white' }} />
                        </div>
                        <p>4 secret tools for stock selection.</p>
                    </div>

                    <div className='flex items-center bg-white shadow-xl w-full md:w-[60%] p-4 gap-4 font-bold'>
                        <div className='bg-primary p-[5px] rounded-md '>
                            <TouchAppSharp fontSize='large' sx={{ color: 'white' }} />
                        </div>
                        <p>4 secret tools for stock selection.</p>
                    </div>

                    <div className='flex items-center bg-white shadow-xl w-full md:w-[60%] p-4 gap-4 font-bold'>
                        <div className='bg-primary p-[5px] rounded-md '>
                            <BrandingWatermark fontSize='large' sx={{ color: 'white' }} />
                        </div>
                        <p>4 secret tools for stock selection.</p>
                    </div>
                </div>
            </div>
        </Container>
    )
}

export default Section1