import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';

export default function ResponsiveDrawer() {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Divider />
      <List>
      <div className='flex flex-col gap-4 items-center w-full'>
        <button className='w-full text-xl text-primary font-bold'>Home</button>
        <button className='w-full text-xl text-gray-406 '>Option</button>
        <button className='w-full text-xl text-gray-406 '>Brokerage comparision</button>
        <button className='w-full text-xl text-gray-406 '>Finclub</button>
        <button className='w-full text-xl text-gray-406 '>Product</button>

      </div>
      </List>
    </Box>
  );

  return (
    <div className='flex flex-row-reverse lg:flex-row justify-between px-4 md:px-8 lg:px-16 py-2  '>
      <h1 className='text-2xl mr-8 md:mr-0 font-bold  text-primary'>Finnovationz</h1>
      <div className='hidden lg:flex gap-6'>
        <button className='text-xl text-primary font-bold border-b-2 border-secondary'>Home</button>
        <button className='text-xl text-gray-406 '>Option</button>
        <button className='text-xl text-gray-406 '>Brokerage comparision</button>
        <button className='text-xl text-gray-406 '>Finclub</button>
        <button className='text-xl text-gray-406 '>Product</button>

      </div>
      <div className='flex lg:hidden'>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}><MenuIcon/></Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
      </div>
    </div>
  );
}
