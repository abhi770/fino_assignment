/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#352A9A',
        'secondary': '#FABF35',
      }
    },
  },
  plugins: [],
}

